" Omni Completion
augroup OmniCompletion
    autocmd!
    autocmd VimEnter,BufWinEnter * setlocal omnifunc=python3complete#Complete
augroup END

" dense-analysis/ale
if isdirectory(expand('~/.vim/plugged/ale'))
    let g:ale_linters = {
        \ 'python': ['pylint']
    \ }
endif
