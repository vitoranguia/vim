" Markdown Converter
" See https://gist.github.com/dashed/6714393
function! MarkdownConverter()
    let l:create = system('mkdir -p /tmp' . expand('%:p:h'))
    let l:converter = system('pandoc -s -f markdown ' . expand('%:p') .
    \ ' -c ~/.vim/github-pandoc.css -o /tmp' . expand('%:p:r') . '.html')
endfunction

augroup MarkdownConverter
    autocmd!
    autocmd VimEnter,BufWinEnter,BufWritePost * call MarkdownConverter()
augroup END

function! MarkdownOpen()
   let l:open = system('x-www-browser /tmp' . expand('%:p:r') . '.html')
endfunction

nmap <Leader>m :call MarkdownOpen()<CR>

