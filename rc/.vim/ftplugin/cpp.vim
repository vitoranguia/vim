" dense-analysis/ale
if isdirectory(expand('~/.vim/plugged/ale'))
    let g:ale_linters = {
        \ 'cpp': ['gcc', 'cpplint']
    \ }
endif
