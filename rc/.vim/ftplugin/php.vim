" dense-analysis/ale
if isdirectory(expand('~/.vim/plugged/ale'))
    let g:ale_php_phpcs_standard = 'psr2'
    let g:ale_linters = {
        \ 'php': ['php', 'phpcs']
    \ }
endif
