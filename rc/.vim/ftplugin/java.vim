" dense-analysis/ale
if isdirectory(expand('~/.vim/plugged/ale'))
    let g:ale_linters = {
        \ 'java': ['javac']
    \ }
endif
