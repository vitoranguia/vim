" junegunn/vim-plug
if ! isdirectory(expand('~/.vim/plugged'))
    silent execute '!curl -fLo ~/.vim/autoload/plug.vim --create-dirs'
    \ ' https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
    augroup VimPlug
        autocmd!
        autocmd VimEnter * PlugInstall
    augroup END
endif
call plug#begin('~/.vim/plugged')
Plug 'ap/vim-css-color', { 'for': ['css'] }
" tags
Plug 'majutsushi/tagbar'
Plug 'lvht/tagbar-markdown', { 'for': ['markdown'] }
" git
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'
" snippets and auto completion
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'sudar/vim-arduino-snippets', { 'for': ['arduino'] }
" style guide
Plug 'dense-analysis/ale'
Plug 'editorconfig/editorconfig-vim'
" syntax
Plug 'sheerun/vim-polyglot'
Plug 'Raimondi/delimitMate'
Plug 'Valloric/MatchTagAlways', { 'for': ['html', 'xml'] }
" theme
Plug 'tomasr/molokai'
Plug 'Yggdroot/indentLine'
call plug#end()

" syntax
filetype indent plugin on
set omnifunc=syntaxcomplete#Complete
set completeopt=longest,menuone
set colorcolumn=80
set cursorline
syntax enable
set linebreak
set term=xterm-256color
set t_Co=256
set mouse=a
set number
set nowrap
syntax on
" spell
set complete+=kspell
set spell spelllang=en
" indent
set autoindent
set smartindent
" status bar
set laststatus=2
set showmode
set showcmd
set ruler
set wildmenu
set wildmode=list:longest,full
" find
set ignorecase
set incsearch
set smartcase
set hlsearch
nmap <Leader>f :let @/ = ''<CR>
" .swn .swo .swp
set undodir=~/.vim/.undo//
set backupdir=~/.vim/.backup//
set directory=~/.vim/.swp//

" netrw
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_winsize = 25
nmap <Leader>n :Lexplore<CR>

" majutsushi/tagbar
if isdirectory(expand('~/.vim/plugged/tagbar'))
    let g:tagbar_width = 25
    let g:tagbar_autofocus = 1
    let g:tagbar_autoclose = 1
    nmap <Leader>t :TagbarToggle<CR>
endif

" SirVer/ultisnips
if isdirectory(expand('~/.vim/plugged/ultisnips'))
    let g:UltiSnipsListSnippets = '<c-l>'  " Default is <c-tab>
    let g:UltiSnipsExpandTrigger = '<c-j>' " Default is <tab>
endif

" tomasr/molokai
if isdirectory(expand('~/.vim/plugged/molokai'))
    colorscheme molokai
endif

" Universal ctags
if executable('ctags')
    function! BuildTags()
        if (isdirectory(expand('.git')))
            let l:tags = system('ctags -R .')
        endif
    endfunction

    augroup BuildTags
        autocmd!
        autocmd BufWinEnter,BufWritePost * call BuildTags()
    augroup END

    command! -nargs=0 BuildTags call BuildTags()
    nmap <Leader>b :call BuildTags()<CR>
endif
