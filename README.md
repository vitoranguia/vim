# Vim 

My simple configuration [Vim](https://www.vim.org), 
see [Xterm](https://gitlab.com/vitoranguia/xterm) and 
[Copy and paste](https://gitlab.com/vitoranguia/copy-and-paste)

![Print screen](https://pixelfed.social/storage/m/113a3e2124a33b1f5511e531953f5ee48456e0c7/85c26953a179a0e533565ce79e82deb9724813d5/KHkwmgJfcGypZgiDsk6fI1cZtm2cZH8sckMhwvSM.png)

Install

```
# apt install -y vim-nox universal-ctags editorconfig git curl 
```

```
$ cp -r rc/* "$HOME"/
```

Set as default

```
# update-alternatives --config editor
```

## Custom 

EditorConfig `"$HOME"/.editorconfig`

Universal Ctags `"$HOME"/.ctags.d/`

Vim `"$HOME"/.vimrc` and `"$HOME"/.vim/`

<kbd>\\</kbd> is Leader key

<kbd>Leader</kbd> + <kbd>f</kbd> clear highlight search 

<kbd>Leader</kbd> + <kbd>n</kbd> toggle explorer bar

<kbd>Leader</kbd> + <kbd>t</kbd> toggle tag bar

<kbd>Ctrl</kbd> + <kbd>j</kbd> expand snippet

<kbd>Ctrl</kbd> + <kbd>l</kbd> list snippet

## Optional

C/C++ (`Python` required)

```
# apt install -y gcc g++ cmake
```

```
$ pip3 install --user cpplint
```

Java

```
# apt install -y default-jre default-jdk gradle maven
```

Markdown ([github-pandoc.css](https://gist.github.com/dashed/6714393) required)

```
# apt install -y pandoc
```

<kbd>Leader</kbd> + <kbd>m</kbd> preview markdown in web browser

Node/JavaScript

```
# apt install -y nodejs npm
```

```
$ npm install -g standard
```

PHP

```
# apt install -y php composer php-xdebug graphviz php-imagick \
php-mbstring php-curl php-soap php-xml php-mbstring php-mysql
```

```
$ composer global require squizlabs/php_codesniffer
```

Python

```
# apt install -y python3-pip python3-setuptools python3-wheel \
python3-venv python3-tk python3-dev
```

```
$ pip3 install --user pylint
```

Shell

```
# apt install -y shellcheck
```

Vim (`Python` required)

```
$ pip3 install --user vim-vint
```
